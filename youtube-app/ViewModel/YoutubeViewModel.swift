//
//  YoutubeViewModel.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 29/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class YoutubeViewModel {
    let API_URL = "https://www.googleapis.com/youtube/v3"
    let API_KEY = "AIzaSyBWPL6CMkxLYezILyXnbiKwhFSYzA88CMA"
    
    var API_GETVIDEO: String?
    var API_GETCHANNEL: String?
    var API_GETSEARCHVIDEO: String?
    var part:String?
    var partFull:String?
    var chart:String?
    var regionCode:String?
    var fields:String?
    
    var video: Video?
    //
    var videoLists = [Video]()
    var videoSuggestLists = [Video]()
    
    init() {
        API_GETVIDEO = API_URL + "/videos"
        API_GETCHANNEL = API_URL + "/channels"
        API_GETSEARCHVIDEO = API_URL + "/search"
        
        part = "snippet"
        partFull = "snippet,contentDetails,statistics"
        chart = "mostPopular"
        regionCode = "VN"
        //
        
    }
    
    func fetchDataVM(completion: @escaping (_ videos: [Video])->Void) {
        fetchYoutubeAPIVideos() {
            videoLists in
            DispatchQueue.main.async {
                self.fetchYoutubeAPIChannelImage() {
                    channelListsWithChannelIcon in
                    DispatchQueue.main.async {
                        
                        completion(channelListsWithChannelIcon)
                        
                        //print("FETCH ALL DATA SUCCESS!")
                    }

                }
            }
        }
    }
    
    // - MARK: FETCH SUGGESTS VIDEO
    
    func fetchYoutubeAPISuggestVideo(videoId: String ,completion: @escaping ([Video])->Void) {
        let params: [String: Any] = [
            "part": part ?? "",
            "relatedToVideoId": videoId,
            "type": "video",
            "key": API_KEY
        ]
        
        Alamofire.request(API_GETSEARCHVIDEO!, method: .get, parameters: params).responseObject {
            (response: DataResponse<VideoResult>) in
            let videoResult = response.result.value
            self.videoSuggestLists = (videoResult?.video)!
            
            completion(self.videoSuggestLists)
            
        }
    }
    
    // - MARK: FETCH VIDEOS
    
    func fetchYoutubeAPIVideos(completion: @escaping ([Video])->Void) {
        let params: [String: Any] = [
            "part": partFull ?? "",
            "chart": chart ?? "",
            "regionCode": regionCode ?? "",
            "key": API_KEY
        ]
        
        Alamofire.request(API_GETVIDEO!, method: .get, parameters: params).responseObject {
            (response: DataResponse<VideoResult>) in
            let videoResult = response.result.value
            self.videoLists = (videoResult?.video)!
            
            completion(self.videoLists)
            
        }
        
    }
    
    // - MARK: FETCH CHANNELS
 
    func fetchYoutubeAPIChannelImage(completion: @escaping ([Video])->Void) {
        let dispatchQueue = DispatchQueue(label: "taskQueue")
        let myGroup = DispatchGroup()
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        dispatchQueue.async {
            for video in self.videoLists {
                myGroup.enter()
                let paramsChannel: [String: Any] = [
                    "part": self.partFull ?? "",
                    "id": video.channelId ?? "",
                    "key": self.API_KEY
                ]
                //request channel icon
                Alamofire.request(self.API_GETCHANNEL!, method: .get, parameters: paramsChannel).responseObject {
                    (response: DataResponse<ChannelResult>) in
                    let channelResult = response.result.value
                    video.channel = channelResult?.channel![0]
                    //signal here
                    dispatchSemaphore.signal()
                    myGroup.leave()
                    
                    //print("Finish channel image url!")
                }
                
                dispatchSemaphore.wait()
            }
        }
        
        myGroup.notify(queue: dispatchQueue) {
            DispatchQueue.main.async {
                //completion(self.channelIcons)
                completion(self.videoLists)
            }
            //print("Finished all requests.")
        }
    }
    
    // - MARK: FORMAT VIEW
    
    func setFormatViewCount(_ viewCountString: String) -> String {
        let viewCountInt: Double = Double(viewCountString)!
        if viewCountInt > 1000000 {
            let num = viewCountInt / 1000000
            if num.rounded().remainder(dividingBy: 10) == 0 {
                return "\(String(format: "%.f", num))Tr"
            }
            else {
                return "\(String(format: "%0.1f", num))Tr"
            }
        }
        else if viewCountInt > 1000 {
            let num = viewCountInt / 1000
            if num.rounded().remainder(dividingBy: 2) == 0 {
                return "\(String(format: "%.f", num))K"
            }
            else {
                return "\(String(format: "%0.1f", num))K"
            }
        }
        else {
            return viewCountString
        }
    }
    
    func setFormatPublishDate(_ publishAt: String) -> String {
        return ""
    }
    
    func setFormatVideoDuration(_ duration: String) -> String {
        let splitString = duration.components(separatedBy: "PT")
        let durationString = splitString[1]
        let index = durationString.index(of: "S") ?? durationString.endIndex
        
        let realDuration = durationString[..<index].components(separatedBy: "M")
        if realDuration[1].isEmpty {
            return "\(realDuration[0]):00  "
        }
        else {
        //let leftRealDuration = realDuration[0].components(separatedBy: "H")
        //if !leftRealDuration[0].isEmpty {
        //    return "\(leftRealDuration[0]):\(realDuration[0]):\(realDuration[1])"
        //}
        //else {
            return "\(realDuration[0]):\(realDuration[1])  "
        //}
        }
    }
    
    //https://www.googleapis.com/youtube/v3/channels?part=snippet&id=CHANNEL_ID&fields=items%2Fsnippet%2Fthumbnails&key=AIzaSyBWPL6CMkxLYezILyXnbiKwhFSYzA88CMA
    //UCGSQsJGuvdtgABsSOp6Xbcg
    //
}
