//
//  Channel.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 30/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

class ChannelIcon: Mappable {
    var url: String?
    //var width: String?
    //var height: String?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        url <- map["high.url"]
        //url <- map["snippet.thumbnails.high.width"]
        //url <- map["snippet.thumbnails.high.height"]
    }
    
}

class Channel: Mappable {
    var viewCount: String?
    var subscriberCount: String?
    var videoCount: String?
    
    var channelIcon : ChannelIcon?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        viewCount <- map["statistics.viewCount"]
        subscriberCount <- map["statistics.subscriberCount"]
        videoCount <- map["statistics.videoCount"]
        channelIcon <- map["snippet.thumbnails"]
    }
    
}

class ChannelResult: Mappable {
    var channel: [Channel]?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        channel <- map["items"]
    }
    
    
    
}
