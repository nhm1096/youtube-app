//
//  VideoResult.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 30/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

class VideoResult: Mappable {
    var totalResults: String?
    var resultsPerPage: String?
    var video: [Video]?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        totalResults <- map["pageInfo.totalResults"]
        resultsPerPage <- map["pageInfo.resultsPerPage"]
        
        video <- map["items"]
    }
    
    
}
