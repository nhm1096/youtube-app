//
//  Video.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 29/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper

class Thumbnail: Mappable {
    var url:String?
    var width:String?
    var height:String?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        width <- map["width"]
        height <- map["height"]
    }
}

class Video: Mappable {
    var id:String?
    var videoId:String?
    var publishAt:String?
    var channelId:String?
    var title:String?
    var desc:String?
    var channelTitle:String?
    var categoryID:String?
    
    var thumbnail:Thumbnail?
    var duration:String?
    
    var viewCount:String?
    var likeCount:String?
    var dislikeCount:String?
    
    var channel: Channel?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        videoId <- map["id.videoId"]
        publishAt <- map["snippet.publishedAt"]
        channelId <- map["snippet.channelId"]
        title <- map["snippet.title"]
        desc <- map["snippet.description"]
        channelTitle <- map["snippet.channelTitle"]
        categoryID <- map["snippet.categoryId"]
        
        thumbnail <- map["snippet.thumbnails.high"]
        
        duration <- map["contentDetails.duration"]
        
        viewCount <- map["statistics.viewCount"]
        likeCount <- map["statistics.likeCount"]
        dislikeCount <- map["statistics.dislikeCount"]
    }
    
}
