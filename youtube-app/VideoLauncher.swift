//
//  VideoLauncher.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 01/02/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import UIKit

class VideoLauncher: NSObject {
    func showVideoLauncher() {
        //print("showing video player animation")
        if let keyWindow = UIApplication.shared.keyWindow {
            let view = UIView(frame: keyWindow.frame)
            view.backgroundColor = UIColor.black
            
            view.frame = CGRect(x: keyWindow.frame.width - 100, y: keyWindow.frame.height - 100, width: 50, height: 50)
            
            keyWindow.addSubview(view)
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                    view.frame = keyWindow.frame
                }, completion: nil)
        }
        
    }
}
