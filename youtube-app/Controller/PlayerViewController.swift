//
//  PlayerViewController.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 01/02/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import UIKit
//import AVFoundation

class PlayerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var player: UIWebView!
    
    //var videoPlayer = AVPlayer()
    
    var youtubeVM = YoutubeViewModel()
    var video: Video?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        
        loadVideo(videoID: (video?.id)!)
        //sloadVideoAVPlayer()
        
    }
    
    func loadVideo(videoID: String){
        let width = self.player.frame.size.width
        let height = self.player.frame.size.height

        
        let youTubeVideoHTML: String = "<!DOCTYPE html><html><head><style>body{margin:0px 0px 0px 0px;}</style></head> <body> <div id=\"player\"></div> <script> var tag = document.createElement('script'); tag.src = \"http://www.youtube.com/player_api\"; var firstScriptTag = document.getElementsByTagName('script')[0]; firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); var player; function onYouTubePlayerAPIReady() { player = new YT.Player('player', { width:'%0.0f', height:'%0.0f', playerVars: {playsinline:1}, videoId:'%@', events: { 'onReady': onPlayerReady, } }); } function onPlayerReady(event) { event.target.playVideo(); } </script> </body> </html>"
        let htmlCode: String = String(format: youTubeVideoHTML, width, height, videoID)

        
        player.allowsInlineMediaPlayback = true
        player.mediaPlaybackRequiresUserAction = false
        player.scrollView.isScrollEnabled = false
        player.scrollView.bounces = false
        
        player.loadHTMLString(htmlCode, baseURL: Bundle.main.resourceURL)
        
        youtubeVM.fetchYoutubeAPISuggestVideo(videoId: videoID) { (videoSuggestLists) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoIndex = indexPath.row - 2
        if videoIndex >= 0 {
            let video: Video = youtubeVM.videoSuggestLists[videoIndex]
            loadVideo(videoID: video.videoId!)
        }
        self.tableView.scrollsToTop = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return youtubeVM.videoSuggestLists.count + 2 //for header + channel
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! CustomTableViewCell
            let viewString = youtubeVM.setFormatViewCount((video?.viewCount!)!)
            cell.initPlayer((video?.title)!, "\(viewString) views", (video?.likeCount)!, (video?.dislikeCount)!)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "channel") as! CustomTableViewCell
            let channel = video?.channel
            let channelImageURL = URL(string: (channel?.channelIcon?.url)!)
            let subscribeString = youtubeVM.setFormatViewCount((channel?.subscriberCount)!)
            
            cell.initPlayerChannel(channelImageURL!, (video?.channelTitle)!, "\(subscribeString) subscribers")
            cell.customizeChannelImage()
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "body") as! CustomTableViewCell
            let videoIndex = indexPath.row - 2
            
            let video = youtubeVM.videoSuggestLists[videoIndex]
            //print(video.viewCount)
            let imageURL = URL(string: (video.thumbnail?.url)!)
            //let viewString = youtubeVM.setFormatViewCount(video.viewCount!)
            //let durationString = youtubeVM.setFormatVideoDuration(video.duration!)
            
            cell.initSuggestVideos(imageURL!, video.title!, video.channelTitle!, "999K views", "00:00  ")
            cell.customizeDuration()
            return cell
        }
        
    }
    
    //func loadVideoAVPlayer() {
    //let url = "https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4"
    //        print(url)
    //        if let urlString = URL(string: url) {
    //            videoPlayer = AVPlayer(url: urlString)
    //            let videoLayer = AVPlayerLayer(player: videoPlayer)
    //            videoLayer.frame = CGRect(x: 0, y: 0, width: self.player.frame.width, height: self.player.frame.height)
    //            videoLayer.videoGravity = AVLayerVideoGravity.resizeAspect
    //
    //            self.player.layer.addSublayer(videoLayer)
    //
    //            videoPlayer.play()
    //        }
    //}
}
