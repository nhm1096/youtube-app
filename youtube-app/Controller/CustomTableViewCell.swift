//
//  CustomTableViewCell.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 30/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import UIKit
import Kingfisher

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var videoDuration: UILabel!
    @IBOutlet weak var channelImage: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoDecription: UILabel!
    ///
    //playerView
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var dislikeCount: UILabel!
    //
    //playerViewChannel
    @IBOutlet weak var channelTitle: UILabel!
    @IBOutlet weak var channelSub: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func customizeChannelImage(){
        // Initialization code
        self.channelImage.layer.cornerRadius = self.channelImage.frame.height / 2
        self.channelImage.clipsToBounds = true
    }
    
    func customizeDuration(){
        self.videoDuration.layer.cornerRadius = self.videoDuration.frame.height / 4
        self.videoDuration.clipsToBounds = true
    }
    
    func initSuggestVideos(_ imageResource: URL, _ title: String, _ channelTitle: String, _ viewCount: String, _ duration: String) {
        self.videoImage.kf.setImage(with: imageResource)
        self.videoTitle.text = title
        self.channelTitle.text = channelTitle
        self.videoDecription.text = viewCount
        self.videoDuration.text = duration
    }
    
    func initPlayer(_ videoTitle: String, _ videoDesc: String, _ likeCount: String, _ dislikeCount: String) {
        self.videoTitle.text = videoTitle
        self.videoDecription.text = videoDesc
        self.likeCount.text = likeCount
        self.dislikeCount.text = dislikeCount
    }
    
    func initPlayerChannel(_ channelResource: URL, _ channelTitle: String, _ channelSub: String) {
        self.channelImage.kf.setImage(with: channelResource)
        self.channelTitle.text = channelTitle
        self.channelSub.text = channelSub
    }
    
    func initVideo(imageResource: URL, videoDuration: String, channelResource: URL, videoTitle: String, videoDescription: String) {
        self.videoImage.kf.setImage(with: imageResource)
        self.videoDuration.text = videoDuration
        self.channelImage.kf.setImage(with: channelResource)
        self.videoTitle.text = videoTitle
        self.videoDecription.text = videoDescription
        
    }

}
