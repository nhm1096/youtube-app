//
//  CustomTabBarController.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 30/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {

    @IBOutlet var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = customView
        self.navigationController?.navigationBar .addSubview(customView)
    }

}
