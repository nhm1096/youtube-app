//
//  HomeController.swift
//  youtube-app
//
//  Created by Nguyễn Minh on 30/01/2018.
//  Copyright © 2018 Nguyễn Minh. All rights reserved.
//

import UIKit
import Kingfisher



class HomeController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let youtubeVM = YoutubeViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        fetchDataAPI()
    }
    
    func fetchDataAPI() {
        youtubeVM.fetchDataVM {
            videos in
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //youtubeVM.videoLists[indexPath.row].channel = youtubeVM.channelIcons[indexPath.row]
        performSegue(withIdentifier: "playSegue", sender: youtubeVM.videoLists[indexPath.row])
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "playSegue" {
            let playerVC = segue.destination as! PlayerViewController
            playerVC.video = sender as? Video
 
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return youtubeVM.videoLists.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomTableViewCell
        
        let video = youtubeVM.videoLists[indexPath.row]
        //let channel = youtubeVM.channelIcons[indexPath.row]
        let channel: ChannelIcon = (youtubeVM.videoLists[indexPath.row].channel?.channelIcon!)!
        
        
        let viewCountString = youtubeVM.setFormatViewCount(video.viewCount!)
        let durationString = youtubeVM.setFormatVideoDuration(video.duration!)
        //print(video.publishAt!)
        //print(video.id!)
        
        let url = URL(string: video.thumbnail?.url ?? "")
        let channelUrl = URL(string: channel.url!)
        
        cell.initVideo(imageResource: url!, videoDuration: durationString, channelResource: channelUrl!, videoTitle: video.title!, videoDescription: "\(video.channelTitle ?? "not found")・\(viewCountString) views")
        
        cell.customizeChannelImage()
        cell.customizeDuration()
        return cell
    }
    
    

}
